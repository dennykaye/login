package com.example.androidstudio.everboy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText uname, upass;
    Button clk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        uname = (EditText) findViewById(R.id.editText2);
        upass = (EditText) findViewById(R.id.editText4);
        clk = (Button) findViewById(R.id.button);
    }

    public void movepage(View v){
        String stname = uname.getText().toString();
        String stupass = upass.getText().toString();

        if (stname.equals("admin") && stupass.equals("admin")){
            Intent in = new Intent(MainActivity.this,SubActivity.class);
            startActivity(in);
        }
        else if(stname.equals("") || stupass.equals("")){
            Toast.makeText(getBaseContext(),"Enter username or password",Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getBaseContext(),"Invalid username or password",Toast.LENGTH_SHORT).show();
        }

    }
}
